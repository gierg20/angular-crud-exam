export class User {
  _id?: string;
  username?: string;
  email?: string;
  province?: string;
  city?: string;
  status?: string;
  role?: string;
}
