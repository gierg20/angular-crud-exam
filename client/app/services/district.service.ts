import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Province } from './../shared/models/region.model';

@Injectable()
export class DistrictService {

    private headers = new Headers();
    private option: RequestOptions = new RequestOptions({ withCredentials: true, headers: this.headers });
    apiRoot: any = 'http://api.cyber313.com/wilayah';
    apiUrl: string;

    constructor(private http: Http, private httpClient: HttpClient) { }

    getProvince(): Observable<any[]> {
        this.apiUrl = `${this.apiRoot}/provinsi`;
        return this.http.get(this.apiUrl).map((res: Response) => {
            return res.json();
        });
    }

    getDistrict(parent_id) {
        this.apiUrl = `${this.apiRoot}/kabupaten/${parent_id}`;
        return this.http.get(this.apiUrl).map((res: Response) => {
            return res.json();
        });
    }

    getDistricts(parent_id) {
        this.apiUrl = `${this.apiRoot}/kecamatan/${parent_id}`;
        return this.http.get(this.apiUrl).map((res: Response) => {
            return res.json();
        });
    }
}
