import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { ToastComponent } from '../shared/toast/toast.component';

import { DistrictService } from './../services/district.service';
import { UserService } from '../services/user.service';

import { User } from '../shared/models/user.model';
import { Province, District } from './../shared/models/region.model';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

    user = new User();
    users: User[] = [];
    provinces: Province[] = [];
    district: District[] = [];
    districts: any[] = [];
    responseData: any = [];

    selectedProvince: any;
    selectedDistrict: any;

    isLoading = true;
    isEditing = false;

    usersForm: FormGroup;
    username = new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern('[a-zA-Z0-9_-\\s]*')
    ]);
    email = new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100)
    ]);
    password = new FormControl('', [
        Validators.required,
        Validators.minLength(6)
    ]);
    role = new FormControl('', [
        Validators.required
    ]);

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        public toast: ToastComponent,
        private userService: UserService,
        private districtService: DistrictService
    ) { }

    ngOnInit() {
        this.ngOnInitForm();
        this.getUsers();
        this.getProvince();
    }

    ngOnInitForm() {
        this.usersForm = this.formBuilder.group({
            username: this.username,
            email: this.email,
            // password: this.password,
            role: this.role,
            provincesCtrl: this.role,
            districtCtrl: this.role,
            districtsCtrl: this.role,
        });
    }

    getProvince() {
        this.districtService.getProvince().subscribe(
            data => this.responseData = data,
            error => this.toast.setMessage(error, 'success'),
            () => {
                this.provinces = this.responseData.result.items;
            }
        );
    }

    getDistrict(parent_id) {
        this.districtService.getDistrict(parent_id).subscribe(
            data => this.responseData = data,
            error => this.toast.setMessage(error, 'success'),
            () => {
                this.district = this.responseData.result.items;
            }
        );
    }

    getDistricts(parent_id) {
        this.districtService.getDistricts(parent_id).subscribe(
            data => this.responseData = data,
            error => this.toast.setMessage(error, 'success'),
            () => {
                this.districts = this.responseData.result.items;
            }
        );
    }

    getUsers() {
        this.userService.getUsers().subscribe(
          data => this.users = data.filter(user => user.role !== 'admin'),
          error => console.log(error),
          () => {
            this.isLoading = false;
          }
        );
    }

    enableEditing(user: User) {
        this.isEditing = true;
        this.user = user;
    }

    cancelEditing() {
        this.isEditing = false;
        this.user = new User();
        this.toast.setMessage('user editing cancelled.', 'warning');
        // reload the cats to reset the editing
        this.getUsers();
    }

    addUser() {
        this.userService.register(this.usersForm.value).subscribe(
            res => {
                this.users.push(res);
                this.usersForm.reset();
                this.toast.setMessage('user added successfully.', 'success');
            },
            error => console.log(error)
        );
    }

    editUser(user: User) {
        this.userService.editUser(user).subscribe(
          () => {
            this.isEditing = false;
            this.user = user;
            this.toast.setMessage('user edited successfully.', 'success');
          },
          error => console.log(error)
        );
      }

    deleteUser(user: User) {
        if (window.confirm('Are you sure you want to permanently delete user ' + user.username + ' ?')) {
          this.userService.deleteUser(user).subscribe(
            () => {
              const pos = this.users.map(elem => elem._id).indexOf(user._id);
              this.users.splice(pos, 1);
              this.toast.setMessage('user deleted successfully.', 'success');
            },
            error => console.log(error)
          );
        }
    }
}
