import * as mongoose from 'mongoose';

const districtSchema = new mongoose.Schema({
    code: { type: String, unique: true },
    name: String,
});

const District = mongoose.model('Cat', districtSchema);

export default District;
